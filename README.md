# RP-700 Pos-Printer Checkmk addon

This project contains an addon for RP-700 Pos printer and some other of the same series.

# Bugs / Important stuff

- no bugs known at the moment.

# Building

To build all the plugins, clone [this repository]
and run:

    make all

(Make sure to have make and Python3 installed.)

You can also build a single plugin, e.g.:

    make 

The files (*.mkp) will be written into the `build/` directory.


# Installing

SSH into your Check_MK machine, change to the user of the site you want to install the plugin,
then run:

    mkp install /path/to/plugin.mkp

or use WATO to install.

then configure the scpecial agent for rp-700 printer devices


# License
Apache 2
