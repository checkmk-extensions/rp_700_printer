#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  ____  ____     _____ ___   ___               _       _            
# |  _ \|  _ \   |___  / _ \ / _ \   _ __  _ __(_)_ __ | |_ ___ _ __ 
# | |_) | |_) |____ / / | | | | | | | '_ \| '__| | '_ \| __/ _ \ '__|
# |  _ <|  __/_____/ /| |_| | |_| | | |_) | |  | | | | | ||  __/ |   
# |_| \_\_|       /_/  \___/ \___/  | .__/|_|  |_|_| |_|\__\___|_|   
#                                   |_|                              
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from datetime import date
from datetime import datetime
from dateutil.parser import parse
from cmk.base.check_api import set_item_state, get_item_state
from .agent_based_api.v1.type_defs import HostLabelGenerator
from .agent_based_api.v1 import HostLabel
from .agent_based_api.v1 import *
from .utils import if64, interfaces
import pprint
import json

from typing import (
    Any,
    Dict,
    Mapping,
    MutableMapping,
    Optional,
    Sequence,
    Tuple,
)


def host_labels(section: Dict) -> HostLabelGenerator:
    yield HostLabel(u'printer', u'rp-700')


def parse_rp_700_printer(string_table):
    """
    :param info: dictionary with all data for all checks and subchecks
    :return:
    """
    if string_table:
        return json.loads(str(string_table[0][0]))
    return dict()


register.agent_section(
    name = "rp_700_info",
    parse_function=parse_rp_700_printer,
    host_label_function=host_labels,
)

register.agent_section(
    name = "rp_700_status",
    parse_function=parse_rp_700_printer,
)



def discover_rp_700_info(section):
    yield Service()

def check_rp_700_info(section):
    for info in section:
        if info[0]=="IP Address":
            yield Result(state=State.OK, summary=f"%s: %s http://%s/ " % (info[0], info[1], info[1]))
        else:
            yield Result(state=State.OK, summary=f"%s: %s" % (info[0], info[1]))

register.check_plugin(
    name = "rp_700_info",
    sections=["rp_700_info"],
    service_name="Printer Info",
    discovery_function=discover_rp_700_info,
    check_function=check_rp_700_info,
)


def discover_rp_700_status(section):
    yield Service()

def check_rp_700_status(section):
    for status in section:
        if status[0]=="Cover Is Open" and status[1]=="Yes":
            yield Result(state=State.WARN, summary=f"%s: %s" % (status[0], status[1]))
        elif status[0]=="Cutter Error" and status[1]=="Yes":
            yield Result(state=State.CRIT, summary=f"%s: %s" % (status[0], status[1]))
        elif status[0]=="Paper End" and status[1]=="Yes":
            yield Result(state=State.CRIT, summary=f"%s: %s" % (status[0], status[1]))
        elif status[0]=="Paper Near End" and status[1]=="Yes":
            yield Result(state=State.WARN, summary=f"%s: %s" % (status[0], status[1]))
        elif status[0]=="Off-Line" and status[1]=="Yes":
            yield Result(state=State.CRIT, summary=f"%s: %s" % (status[0], status[1]))
        else:
            yield Result(state=State.OK, summary=f"%s: %s" % (status[0], status[1]))

register.check_plugin(
    name = "rp_700_status",
    sections=["rp_700_status"],
    service_name="Printer Status",
    discovery_function=discover_rp_700_status,
    check_function=check_rp_700_status,
)

