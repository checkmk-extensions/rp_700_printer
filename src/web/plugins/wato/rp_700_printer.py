#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  ____  ____     _____ ___   ___               _       _            
# |  _ \|  _ \   |___  / _ \ / _ \   _ __  _ __(_)_ __ | |_ ___ _ __ 
# | |_) | |_) |____ / / | | | | | | | '_ \| '__| | '_ \| __/ _ \ '__|
# |  _ <|  __/_____/ /| |_| | |_| | | |_) | |  | | | | | ||  __/ |   
# |_| \_\_|       /_/  \___/ \___/  | .__/|_|  |_|_| |_|\__\___|_|   
#                                   |_|                              
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from typing import Any, Dict, List, Optional

from cmk.gui.plugins.wato.datasource_programs import RulespecGroupDatasourceProgramsHardware

import cmk.gui.bi as bi
import cmk.gui.watolib as watolib
from cmk.gui.exceptions import MKUserError
from cmk.gui.i18n import _
from cmk.gui.plugins.metrics.utils import MetricName
from cmk.gui.plugins.wato import (
    HostRulespec,
    IndividualOrStoredPassword,
    monitoring_macro_help,
    rulespec_group_registry,
    rulespec_registry,
    RulespecGroup,
    RulespecSubGroup,
)
from cmk.gui.plugins.wato.utils import PasswordFromStore
from cmk.gui.valuespec import (
    Age,
    Alternative,
    CascadingDropdown,
    Checkbox,
    Dictionary,
    DropdownChoice,
    FixedValue,
    Float,
    Hostname,
    HTTPUrl,
    ID,
    Integer,
    ListChoice,
    ListOf,
    ListOfStrings,
    MonitoringState,
    Password,
    RegExp,
    RegExpUnicode,
    TextAscii,
    TextUnicode,
    Transform,
    Tuple,
)


def _valuespec_special_agents_rp_700_printer():
    return Transform(
        Dictionary(
            title=_("RP-700 Thermal Printer"),
            elements=[
                ("use_hostname",
                 Checkbox(
                    title=_("Use hostname"),
                    label=_("Use hostname instead of IP for check execution. If a special hostname is set this will be overwritten."),
                    default_value=False,
                    true_label=_("use hostname"),
                    false_label=_("use IP"),
                 )),
            ],
            optional_keys=[],
        ),
    )


rulespec_registry.register(
    HostRulespec(
        group=RulespecGroupDatasourceProgramsHardware,
        name="special_agents:rp_700_printer",
        title=lambda: _("RP-700 Thermal Printer"),
        valuespec=_valuespec_special_agents_rp_700_printer,
    ))
